require 'site_prism'


class ConsultaPage < SitePrism::Page
    #element :message_page_result_search, '#center_column > h1'
    element :message_page_result_search, '.page-heading'
    element :message_qtd_result_search, '.heading-counter'
end
